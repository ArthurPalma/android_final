package com.example.minijeucombat;

public class Player {

    String playerName;
    String playerSurname;
    Integer playerAge;
    String playerSex;
    String playerEmail;
    String playerPwd;
    String playerLogin;
    Integer playerScore;

    public Player(){

    }


    public Player(String playerName, String playerSurname, Integer playerAge, String playerSex, String playerEmail, String playerPwd, String playerLogin) {
        this.playerName = playerName;
        this.playerSurname = playerSurname;
        this.playerAge = playerAge;
        this.playerSex = playerSex;
        this.playerEmail = playerEmail;
        this.playerPwd = playerPwd;
        this.playerLogin = playerLogin;
        this.playerScore = 0;
    }


    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerSurname() {
        return playerSurname;
    }

    public Integer getPlayerAge() {
        return playerAge;
    }

    public String getPlayerSex() {
        return playerSex;
    }

    public String getPlayerEmail() {
        return playerEmail;
    }

    public Integer getPlayerScore() {
        return playerScore;
    }

    public String getPlayerPwd() {
        return playerPwd;
    }

    public String getPlayerLogin() {
        return playerLogin;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setPlayerSurname(String playerSurname) {
        this.playerSurname = playerSurname;
    }

    public void setPlayerAge(Integer playerAge) {
        this.playerAge = playerAge;
    }

    public void setPlayerSex(String playerSex) {
        this.playerSex = playerSex;
    }

    public void setPlayerEmail(String playerEmail) {
        this.playerEmail = playerEmail;
    }

    public void setPlayerPwd(String playerPwd) {
        this.playerPwd = playerPwd;
    }

    public void setPlayerLogin(String playerLogin) {
        this.playerLogin = playerLogin;
    }

    public void setPlayerScore(Integer playerScore) {
        this.playerScore = playerScore;
    }

    public String toString() {
        return this.playerName+" : "+playerScore.toString();
    }
}
