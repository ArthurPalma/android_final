package com.example.minijeucombat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Remerciement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remerciement);
    }

    public void Go_Main(View v) {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void Go_Regles(View v) {
        Intent intent=new Intent(this,Regles.class);
        startActivity(intent);
    }
}
