package com.example.minijeucombat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

import static java.lang.Integer.parseInt;


public class Jeux extends AppCompatActivity {

    ImageButton Pierre, Feuille, Ciseaux, Lezard, Spoke;
    TextView Joueur, Adversiare, Resultat, mancheJ, mancheA, ResultatPartie;
    Button Passermanche;
    int coupJoueur=-1;
    int mancheJoueur=0;
    int mancheAdversaire=0;
    int manche=0;
    int bonus = 0;
    double level = 0;
    Random rand=new Random();
    int coupAdversaire=rand.nextInt(5);
    double badluck = Math.random();
    int difficulty = 0;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databseref = database.getReference();
    int ancienScore;
    int newScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeux);

        Pierre = findViewById(R.id.pierre);
        Feuille = findViewById(R.id.feuille);
        Ciseaux = findViewById(R.id.ciseaux);
        Lezard = findViewById(R.id.lezard);
        Spoke = findViewById(R.id.spoke);
        Passermanche = findViewById(R.id.Passermanche);
        Joueur = findViewById(R.id.textView4);
        Adversiare = findViewById(R.id.textView5);
        Resultat = findViewById(R.id.textView3);
        mancheJ = findViewById(R.id.nbMancheJ);
        mancheA = findViewById(R.id.nbMancheA);
        ResultatPartie = findViewById(R.id.resultatPartie);


        Intent fin = getIntent();

        if(fin != null) {

            mUser = fin.getParcelableExtra("User");
            difficulty = fin.getIntExtra("difficulty", 0);
        }
            Passermanche.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ResultatCoup(view);
                }
            });
        }




    public void ValeurCoup(View v){
        if(v.getId()==Pierre.getId()){
            coupJoueur=0;
            Joueur.setText("Pierre");
        }
        else if(v.getId()==Feuille.getId()){
            coupJoueur=1;
            Joueur.setText("Feuille");
        }
        else if(v.getId()==Ciseaux.getId()){
            coupJoueur=2;
            Joueur.setText("Ciseaux");
        }
        else if(v.getId()==Lezard.getId()){
            coupJoueur=3;
            Joueur.setText("Lezard");
        }
        else if(v.getId()==Spoke.getId()){
            coupJoueur=4;
            Joueur.setText("Spoke");
        }
        ValeurCoupAdversiare(v);

    }

    public void ValeurCoupAdversiare(View v){


        switch (difficulty) {
            case 1:
                level = 0.1;
                bonus = 0;
                break;
            case 2:
                level = 0.2;
                bonus = 1;
                break;
            case 3:
                level = 0.3;
                bonus = 2;
                break;
            case 4:
                level = 0.4;
                bonus = 3;
                break;
            case 5:
                level = 0.5;
                bonus = 4;
                break;
            default:
                level = 0;
                bonus = 0;
                Toast.makeText(Jeux.this, "mauvaise valeur", Toast.LENGTH_LONG).show();
        }
        if (level<badluck)
        {
            if(coupJoueur==0){coupAdversaire=1;}
            if(coupJoueur==1){coupAdversaire=2;}
            if(coupJoueur==2){coupAdversaire=0;}
            if(coupJoueur==3){coupAdversaire=0;}
            if(coupJoueur==4){coupAdversaire=3;}

        }
        if(coupAdversaire==0){Adversiare.setText("Pierre");}
        if(coupAdversaire==1){Adversiare.setText("Feuille");}
        if(coupAdversaire==2){Adversiare.setText("Ciseaux");}
        if(coupAdversaire==3){Adversiare.setText("Lezard");}
        if(coupAdversaire==4){Adversiare.setText("Spoke");}
        Feuille.setClickable(false);
        Spoke.setClickable(false);
        Ciseaux.setClickable(false);
        Lezard.setClickable(false);
        Pierre.setClickable(false);
    }



    public void ResultatCoup(View v){

        if (manche<5){
            Feuille.setClickable(true);
            Spoke.setClickable(true);
            Ciseaux.setClickable(true);
            Lezard.setClickable(true);
            Pierre.setClickable(true);
        }
        if((coupJoueur==0 && (coupAdversaire==1 || coupAdversaire==4)) || (coupJoueur==1&&(coupAdversaire==2||coupAdversaire==3)) || (coupJoueur==2&&(coupAdversaire==0||coupAdversaire==4)) || (coupJoueur==3&&(coupAdversaire==1||coupAdversaire==2)) || (coupJoueur==4&&(coupAdversaire==3||coupAdversaire==1))){
            Resultat.setText("Adversaire gagne");
            mancheAdversaire++;
        }
        else if(coupJoueur==coupAdversaire)
        {
            Resultat.setText("egalitée");
        }
        else {
            Resultat.setText("Joueur gagne");
            mancheJoueur++;
        }
        mancheJ.setText(String.valueOf(mancheJoueur));
        mancheA.setText(String.valueOf(mancheAdversaire));
        coupAdversaire=rand.nextInt(5);
        manche++;
        if(manche==5)
        {
            Passermanche.setClickable(false);
            mancheJoueur += bonus;
            Toast.makeText(Jeux.this, "juste pour voir", Toast.LENGTH_LONG).show();
            newScore = mancheJoueur;
            ResultatPartie(v);
            updateData(v);
        }
    }

    public void ResultatPartie(View v){
        mancheJoueur=0;
        mancheAdversaire=0;
        manche =0;
        if(mancheJoueur>mancheAdversaire)
        {
            ResultatPartie.setText("joueur gagne");
        }
        else
        {
            ResultatPartie.setText("Adversaire gagne");
        }

        Feuille.setClickable(false);
        Spoke.setClickable(false);
        Ciseaux.setClickable(false);
        Lezard.setClickable(false);
        Pierre.setClickable(false);

    }

    public void rejouer(View v){

        Feuille.setClickable(true);
        Spoke.setClickable(true);
        Ciseaux.setClickable(true);
        Lezard.setClickable(true);
        Pierre.setClickable(true);
        Passermanche.setClickable(true);
        mancheJ.setText(String.valueOf(mancheJoueur));
        mancheA.setText(String.valueOf(mancheAdversaire));
        Resultat.setText("En attente");
        ResultatPartie.setText("En attente");
    }


    public void updateData(View v) {



        databseref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(Jeux.this, "Debut", Toast.LENGTH_LONG).show();

                ancienScore = dataSnapshot.child("player").child(mUser.getUid()).child("playerScore").getValue(int.class);


                int scoreFinal= ancienScore+newScore;

                databseref.child("player").child(mUser.getUid()).child("playerScore").setValue(scoreFinal);

                Toast.makeText(Jeux.this, "juste pour voir", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Jeux.this, "Aie, échec", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void Go_Menu(View v) {
        Intent intent=new Intent(this,Menu.class);
        startActivity(intent);
    }

    public void Go_Rules(View v) {
        Intent intent=new Intent(this,Regles.class);
        startActivity(intent);
    }


}
