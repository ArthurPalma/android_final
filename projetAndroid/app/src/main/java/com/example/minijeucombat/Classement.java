package com.example.minijeucombat;

import android.content.Intent;
import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class Classement extends AppCompatActivity {

    DatabaseReference databaseReference;
    ListView classement;
    Button retour;
    private FirebaseUser mUser;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classement);
        Intent fin = getIntent();
        if(fin != null) {
            mUser = fin.getParcelableExtra("User");
        }
        databaseReference = FirebaseDatabase.getInstance().getReference("player")
        ;
        classement = findViewById(R.id.classement);
        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, arrayList);
        classement.setAdapter(arrayAdapter);
        databaseReference.orderByChild("playerScore").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot children: dataSnapshot.getChildren()) {
                    String value = children.getValue(Player.class).toString();
                    arrayList.add(value);
                }
                Collections.reverse(arrayList);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        retour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent end =new Intent(Classement.this, Menu.class);
                end.putExtra("User",mUser);
                startActivity(end);
            }
        });
    }


    public void Go_Menu(View v) {
        Intent intent=new Intent(this,Menu.class);
        startActivity(intent);
    }
}
