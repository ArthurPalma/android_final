package com.example.minijeucombat;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Inscription extends AppCompatActivity{

    EditText nom, prenom, email, login, mdp;
    Button valider;
    Spinner age, sexe;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);




        nom = findViewById(R.id.nom);
        prenom = findViewById(R.id.prenom);
        email = findViewById(R.id.mail);
        login = findViewById(R.id.login);
        mdp = findViewById(R.id.mdp);
        valider = findViewById(R.id.valider);
        age = findViewById(R.id.age);
        sexe = findViewById(R.id.sexe);

        mAuth = FirebaseAuth.getInstance();
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPlayer();
            }
        });
    }

    private void addPlayer(){
        final String playername = nom.getText().toString().trim();
        final String playersurname = prenom.getText().toString().trim();
        final String playeremail = email.getText().toString().trim();
        final String playerlogin = login.getText().toString().trim();
        final String playerpwd = mdp.getText().toString().trim();
        final Integer playerage = Integer.parseInt(age.getSelectedItem().toString());
        final String playersexe = sexe.getSelectedItem().toString();

        if (!TextUtils.isEmpty(playername) && !TextUtils.isEmpty(playersurname ) && !TextUtils.isEmpty(playeremail) && !TextUtils.isEmpty(playerlogin) && !TextUtils.isEmpty(playerpwd)){



            Player player = new Player (playername, playersurname, playerage, playersexe, playeremail, playerpwd, playerlogin);

            mAuth.createUserWithEmailAndPassword(playeremail, playerpwd)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {

                                Player player = new Player (playername, playersurname, playerage, playersexe, playeremail, playerpwd, playerlogin);

                                FirebaseDatabase.getInstance().getReference("player")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(player).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(Inscription.this, "Player added", Toast.LENGTH_LONG).show();
                                            startActivity(new Intent(Inscription.this, Regles.class));
                                        } else {
                                            Toast.makeText(Inscription.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            } else {
                                Toast.makeText(Inscription.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            nom.getText().clear();
            prenom.getText().clear();
            email.getText().clear();
            login.getText().clear();
            mdp.getText().clear();
            age.setSelection(0);
            sexe.setSelection(0);

        }


    }

    public void Go_Remerciement(View v) {
        Intent intent=new Intent(this,Remerciement.class);
        startActivity(intent);
    }

    public void fermer(View v) {

        finish() ;
    }
}
