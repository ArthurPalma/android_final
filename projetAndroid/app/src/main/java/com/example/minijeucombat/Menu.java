package com.example.minijeucombat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Menu extends AppCompatActivity {

    Button logout, game, classement;
    Spinner difficulty;
    TextView score, pseudo;
    private FirebaseUser mUser;
    FirebaseAuth mAuth;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databseref = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        logout = findViewById(R.id.deconnexion);
        game = findViewById(R.id.ButtonGame);
        classement = findViewById(R.id.classementButton);
        difficulty = findViewById(R.id.difficulty);
        score = findViewById(R.id.textScore);
        pseudo = findViewById(R.id.textPlayer);

        mAuth = FirebaseAuth.getInstance();


        Intent fin = getIntent();
        if(fin != null) {
            mUser = fin.getParcelableExtra("User");
        }
        databseref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int valeur = dataSnapshot.child("player").child(mUser.getUid()).child("playerScore").getValue(int.class);

                score.setText("meilleur score = " + valeur);

                String login = dataSnapshot.child("player").child(mUser.getUid()).child("playerName").getValue(String.class);
                pseudo.setText(" bienvenue " + login);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Menu.this, "erreur, impossible de récupérer les données", Toast.LENGTH_LONG).show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(Menu.this, MainActivity.class));
            }
        });

        game.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int val = Integer.parseInt(difficulty.getSelectedItem().toString());
                Intent end =new Intent(Menu.this, Jeux.class);
                end.putExtra("User",mUser);
                end.putExtra("difficulty",val);
                startActivity(end);
            }
        });

        classement.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent end =new Intent(Menu.this, Classement.class);
                end.putExtra("User",mUser);
                startActivity(end);
            }
        });

    }
    public void Go_Regles(View v) {
        Intent intent=new Intent(this,Regles.class);
        startActivity(intent);
    }



    public void Go_Main(View v) {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }


}
