package com.example.minijeucombat;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity {

    EditText email, mdp;
    Button connexion;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = findViewById(R.id.login);
        mdp = findViewById(R.id.pwd);
        connexion = findViewById(R.id.connexion);

        mAuth = FirebaseAuth.getInstance();

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connexion(view);
            }
        });

    }

    public void connexion(View v){
        mAuth.signInWithEmailAndPassword(email.getText().toString(), mdp.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser mUser = mAuth.getCurrentUser();
                            Intent menu = new Intent(MainActivity.this, Menu.class);
                            menu.putExtra("User", mUser);
                            startActivity(menu);
                        } else {
                            Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
        email.getText().clear();
        mdp.getText().clear();

    }
    public void Go_Inscription(View v) {
        Intent intent=new Intent(this,Inscription.class);
        startActivity(intent);
    }


}
